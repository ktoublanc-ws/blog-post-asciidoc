= L'Observabilité par la pratique
:toc:

== TLDR;

Blablabla, mon article est long...

- Accès aux sources: https://gitlab.com/wescale/balbalbla

:sectnums:
:sectnumlevels: 1

== Tell me why ?

=== Ain't nothing but a heartache

[source, bash]
----
echo Hello
----

== Tell me why ?

=== Ain't nothin' but a mistake

[plantuml]
----
include::test.puml[]
----

=== I never wanna hear you say


== I want it that way
