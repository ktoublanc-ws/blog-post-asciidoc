DRIVE_LOCATION := /Volumes/GoogleDrive/Mon\ Drive
IMAGE_OUTPUT_DIR := build/img

.PHONY: build
build:
	asciidoctor -a imagesoutdir=$(IMAGE_OUTPUT_DIR) -r asciidoctor-diagram --backend docbook --out-file - observability/observability.adoc > build/observability.docbook
	pandoc --from docbook --to docx --output $(DRIVE_LOCATION)/observability.docx build/observability.docbook

.PHONY: install
install:
	sudo gem install asciidoctor-diagram
	brew install asciidoctor pandoc